import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {User} from './user'

@Component({
  selector: '[jce-user]',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['user']
})
export class UserComponent implements OnInit {
  @Output() deleteEvent = new EventEmitter<User>();
  @Output() editEvent = new EventEmitter<User>();
  user:User;
  tempUser:User = {email:null,name:null};
  isEdit : boolean = false;
  editButtonText;
  constructor() { }

  sendDelete(){
    this.deleteEvent.emit(this.user);
  }

  toggleEdit(){
     //update parent about the change
     this.isEdit = !this.isEdit; 
     this.isEdit ? this.editButtonText = '<i class="fa fa-floppy-o" aria-hidden="true"></i>' : this.editButtonText = '<i class="fa fa-pencil" aria-hidden="true"></i>';    
     if(this.isEdit){
       this.tempUser.email = this.user.email;
       this.tempUser.name = this.user.name;
     }else{
       this.editEvent.emit(this.user);
     }
  }

  ngOnInit() {
     this.isEdit ? this.editButtonText = '<i class="fa fa-floppy-o" aria-hidden="true"></i>' : this.editButtonText = '<i class="fa fa-pencil" aria-hidden="true"></i>';   
  }

}
