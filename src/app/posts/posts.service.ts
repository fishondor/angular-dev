import { Injectable } from '@angular/core';
import { AngularFire } from 'angularfire2'; 
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class PostsService {

  postsObservable;

  constructor(private af:AngularFire) { }

  addPost(post){
    this.postsObservable.push(post);
  }

  deletePost(post){
    this.af.database.object('/posts/' + post.$key).remove();
  }

  updatePost(post){
    let post1 = {title : post.title, body : post.content};
    this.af.database.object('/posts/' + post.$key).update(post1);
  }

  getPosts(){
    this.postsObservable = this.af.database.list('/posts').map(
      posts => {
        posts.map(
          post => {
            post.userNames = [];
            for(var p in post.users){
              post.userNames.push(
                this.af.database.object('/users/' + post.users[p])
              );
            }
          }
        )
        return posts;
      }
    );
    
    return this.postsObservable;
  }

}
