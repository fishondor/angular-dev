import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

import { PostsService } from './posts.service';
import { UsersService } from '../users/users.service';
import { PostComponent } from '../post/post.component';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css'],
  providers: [PostsService]
})
export class PostsComponent implements OnInit {

  @ViewChild('modal')
  modal: ModalComponent;

  @ViewChild('deleteModal')
  deleteModal: ModalComponent;

  posts;

  currentPost = {title: '', id: ''};

  isLoading: boolean = true;

  modalTitle: string = '';

  cssClass: string = '';

  model = {title: '', body: '', users: []};

  action;

  users;

  select(post){
		this.currentPost = post; 
    console.log(this.currentPost);
  }

  deletePost(id){
    this.posts.splice(
      this.getPostIndexById(id),
      1
    )
  }

  editPost(event){
    this.currentPost = Object.assign({}, this.getPostById(event.postId));
    this.model = this.getPostById(event.postId);
    this.modalTitle = "Edit post";
    this.action = this.close;
    this.modal.open();
  }

  newPost(){
    this.model.title = '';
    this.model.body = '';
    this.model.users = [];
    this.modalTitle = "New post";
    this.action = this.savePost;
    this.modal.open();
  }

  confirmDelete(event){
    this.currentPost = this.getPostById(event.postId);
    this.deleteModal.open('sm');
  }

  savePost(){
    this._postsService.addPost(this.model);
    this.modal.dismiss();
  }

  close(){
    this.modal.dismiss();
  }

  cancel(){
    if(this.currentPost)
      this.posts.splice(this.getPostIndexById(this.currentPost.id), 1, Object.assign({}, this.currentPost));
    this.modal.dismiss();
  }

  getPostIndexById(id){
    for(var i = 0; i < this.posts.length; i++){
      if(this.posts[i].id == id)
        return i;
    }
  }

  getPostById(id){
    for(var i = 0; i < this.posts.length; i++){
      if(this.posts[i].id == id)
        return this.posts[i];
    }
  }

  constructor(private _postsService: PostsService,
              private _usersService: UsersService) { 
    //this.posts = _postsService.getPosts();
  }

  ngOnInit() {
    this._postsService.getPosts()
			    .subscribe(posts => {this.posts = posts;
            console.log(posts);
            
                               this.isLoading = false});
    this._usersService.getUsers()
          .subscribe(users => {this.users = users});
  }

}