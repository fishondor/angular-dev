import { Injectable } from '@angular/core';
import { AngularFire } from 'angularfire2'; 
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class UsersService {

  users = [];
  
  usersObservable;

  constructor(private af:AngularFire) { }

  addUser(user){
    this.usersObservable.push(user);
  }

  deleteUser(user){
    this.af.database.object('/users/' + user.$key).remove();
    console.log('/users/'+user.$key);
  }

  updateUser(user){
    let user1 = {name:user.name, email : user.email};
    console.log(user1);
    this.af.database.object('/users/' + user.$key).update(user1);
  }

  getUsers(){
    console.log("getting users");
		this.usersObservable = this.af.database.list('/users');
    return this.usersObservable;
	}

  updateUsers(users){
    this.users = users;
  }

  getUserName(userId){
    for(var i = 0; i < this.users.length; i++){
      if(userId == this.users[i].id)
        return this.users[i].name;
    }
    return "Not known";
  }

}
