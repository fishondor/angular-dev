import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Post } from './post';
import { PostsService } from '../posts/posts.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  @Input() post : Post;
  @Output() deleteEvent = new EventEmitter();
  @Output() editEvent = new EventEmitter();
  isEdit : boolean = false;
  editButtonText;

  constructor(private _postsService: PostsService) { }

  sendDelete(postId){
    this.deleteEvent.emit({
      postId: postId
    });
  }

  editPost(postId){
    this.editEvent.emit({
      postId: postId
    });
  }

  toggleEdit(){
     //update parent about the change
     this.isEdit = !this.isEdit; 
     this.isEdit ? this.editButtonText = '<i class="fa fa-floppy-o" aria-hidden="true"></i>' : this.editButtonText = '<i class="fa fa-pencil" aria-hidden="true"></i>';    
  }

  ngOnInit() {
  }

}
