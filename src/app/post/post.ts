export class Post{
    title : String;
    content : String;
    author : String;

    constructor(title, content, author){
        this.title = title;
        this.content = content;
        this.author = author;
    }

    getTitle(){
        return this.title;
    }

    getContent(){
        return this.content;
    }

    getAuthor(){
        return this.author;
    }

    toJSON(){
        return {
            title: this.title,
            content: this.content,
            author: this.author
        }
    }

}